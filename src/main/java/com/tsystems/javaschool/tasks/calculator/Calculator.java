package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collections;
import java.util.Locale;
import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {

    // list of available operators
    private final String OPERATORS = "+-*/";
    // temporary stack that holds operators, functions and brackets
    private Stack<String> stackOperations = new Stack<>();
    // stack for holding expression converted to reversed polish notation
    private Stack<String> stackRPN = new Stack<>();
    // stack for holding the calculations result
    private Stack<String> stackResult = new Stack<>();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement==null) return null;
        try {
            parse(statement);
            compute();
            Double doubleAnswer = Double.parseDouble(stackResult.pop());
            if (doubleAnswer.isInfinite()) return null;
            if (doubleAnswer - doubleAnswer.intValue() > 0) {
                return new DecimalFormat("#.####", new DecimalFormatSymbols(Locale.ENGLISH)).format(doubleAnswer);
            } else {
                return String.valueOf(doubleAnswer.intValue());
            }
        } catch (Exception e) {return null;}
    }

    /**
     * Parse input expression and fill stackRPN.
     * @param expression mathematical statement containing digits, '.' (dot) as decimal mark,
     *                   parentheses, operations signs '+', '-', '*', '/'<br>
     *                   Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @throws IllegalArgumentException
     */
    private void parse(String expression) throws IllegalArgumentException {
        // cleaning stacks
        stackOperations.clear();
        stackRPN.clear();
        // prepare expression
        expression = expression.replace(" ", "").replace("(-", "(0-")
                .replace(",-", ",0-");
        if (expression.charAt(0) == '-') {
            expression = "0" + expression;
        }
        // splitting input string into tokens
        StringTokenizer stringTokenizer = new StringTokenizer(expression,
                OPERATORS + "()", true);
        //shunting-yard algorithm
        int openBrackets = 0, closeBrackets = 0;
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (isOpenBracket(token)) {
                stackOperations.push(token);
                openBrackets++;
            } else if (isCloseBracket(token)) {
                while (!stackOperations.empty()
                        && !isOpenBracket(stackOperations.lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
                stackOperations.pop();
                if (!stackOperations.empty()) {
                    stackRPN.push(stackOperations.pop());
                }
                closeBrackets++;
            } else if (isNumber(token)) {
                stackRPN.push(token);
            } else if (isOperator(token)) {
                while (!stackOperations.empty()
                        && isOperator(stackOperations.lastElement())
                        && getPrecedence(token) <= getPrecedence(stackOperations
                        .lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
                stackOperations.push(token);
            } else {
                throw new IllegalArgumentException();
            }
        }
        if (openBrackets != closeBrackets) throw new IllegalArgumentException();
        while (!stackOperations.empty()) {
            stackRPN.push(stackOperations.pop());
        }

        // reverse stack
        Collections.reverse(stackRPN);
    }

    /**
     * Compute result of exprassion and place it into stackResult.
     * @throws IllegalArgumentException
     */
    private void compute() throws IllegalArgumentException {
        while (!stackRPN.empty()) {
            String token = stackRPN.pop();
            if (isNumber(token)) {
                stackResult.push(token);
            } else if (isOperator(token)) {
                Double rightOperand = Double.parseDouble(stackResult.pop());
                Double leftOperand = Double.parseDouble(stackResult.pop());
                Double result;
                switch (token) {
                    case "+":
                        result = leftOperand + rightOperand;
                        break;
                    case "-":
                        result = leftOperand - rightOperand;
                        break;
                    case "*":
                        result = leftOperand * rightOperand;
                        break;
                    case "/":
                        result = leftOperand / rightOperand;
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
                stackResult.push(result.toString());
            }
        }
    }

    //Utilities
    private boolean isNumber(String token) {
        try { Double.parseDouble(token); }
        catch (Exception e) {return false;}
        return true;
    }

    private boolean isOpenBracket(String token) {
        return token.equals("(");
    }

    private boolean isCloseBracket(String token) {
        return token.equals(")");
    }

    private boolean isOperator(String token) {
        return OPERATORS.contains(token);
    }

    private byte getPrecedence(String token) {
        if (token.equals("+") || token.equals("-")) return 1;
        return 2;
    }

}
