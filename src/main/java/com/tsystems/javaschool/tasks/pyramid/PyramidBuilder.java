package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        // Check possibility to create pyramid
        if (inputNumbers == null || inputNumbers.size() == 0) throw new CannotBuildPyramidException();

        for (Integer number : inputNumbers) {
            if (number == null) throw new CannotBuildPyramidException();
        }

        int height = 0;
        for (int i = 0, sum = 0; ; i++, sum += i, height++) {
            if (sum > inputNumbers.size()) throw new CannotBuildPyramidException();
            if (sum == inputNumbers.size()) break;
        }

        // Create blank pyramid
        int width = height * 2 - 1;
        if (height < 1 || height < 1) throw new CannotBuildPyramidException(); //Check overflow exception
        int[][] pyramid = new int[height][width];

        for (int y = 0; y < height; y++) {
            for (int x = width / 2 + 1, n = 0; (x >= 0) && (n <= y + 1); x--) {
                if ( (y + x + height) % 2 == 1) {
                    pyramid[y][x] = 1;
                    pyramid[y][width - 1 - x] = 1;
                    n += 2;
                }
                else  {
                    pyramid[y][x] = 0;
                    pyramid[y][width - 1 - x] = 0;
                }
            }
        }

        //Fill pyramid sorted input
        List<Integer> copyInput = new ArrayList<>(inputNumbers);
        Collections.sort(copyInput);

        int index = 0;
        for (int y = 0; y < pyramid.length; y++) {
            for (int x = 0; x < pyramid[0].length; x++) {
                if (pyramid[y][x] != 0) {
                    pyramid[y][x] = copyInput.get(index++);
                }
            }
        }

        return pyramid;
    }

}
